import Vue from 'vue'
import Vuex from 'vuex'
import AxiosClient from "../plugins/axiosClient";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        navItems: {},
        type_id: '',
        realty_id: '',
        communals: {},
        serviceNames: {
            367: "Мусор",
            168: "Мусор",
            119: "Хол. вода",
            127: "Гор. вода",
            166: "Газ",
            3: "Эл. энергия"
        },
        currentRealty : {}
    },
    mutations: {
        SET_NAV_ITEMS(state, navItems) {
            state.navItems = navItems
        },
        SET_TYPE_ID_AND_REALTY_ID(state, navItems) {
            if (navItems) {
                state.realty_id = navItems.currentRealty.id
                state.type_id = navItems.leftMenu[0].type_id
                state.currentRealty = navItems.currentRealty
            } else {
                state.type_id = ''
            }
        },
        SET_CHANGE_STATE(state, type) {
            state.type_id = type.type_id
        },
        SET_CHANGE_REALTY(state, realty) {
            state.realty_id = realty.realty_id
        },
        CLEAR_COMMUNALS(state) {
            state.communals = {}
        },
        SET_COMMUNALS(state, {data, type_id}) {
            if (type_id)
                if (!(type_id in state.communals)) {
                    state.communals[type_id] = data
                }
        },
        GET_COMMUNAL(state, type_id) {
            if (state.communals) {
                return state.communals[type_id];
            }
        }
    },
    actions: {
        async getNavItems({commit}, id) {
            let parmas = ''
            if (!id)
                parmas = ''
            else
                parmas = '?id=' + id.id
            await AxiosClient.get('/default/communals-list' + parmas)
                .then(response => {
                    if (response.data.data) {
                        commit('SET_NAV_ITEMS', response.data.data)
                        commit('SET_TYPE_ID_AND_REALTY_ID', response.data.data)
                    } else {
                        commit('SET_NAV_ITEMS', [])
                        commit('SET_TYPE_ID_AND_REALTY_ID', null)
                    }
                })
        },
        async setNavItems({commit}, id) {
            if (id)
                await AxiosClient.get('/default/communals-list?id=' + id.id)
                    .then(response => {
                        if (response.data.data) {
                            commit('SET_NAV_ITEMS', response.data.data)
                            commit('SET_TYPE_ID_AND_REALTY_ID', response.data.data)
                            commit('CLEAR_COMMUNALS')
                        } else {
                            commit('SET_NAV_ITEMS', [])
                            commit('SET_TYPE_ID_AND_REALTY_ID', null)
                        }
                    })
        },
        changeState({commit}, type) {
            commit('SET_CHANGE_STATE', type)
        },
        setCommunals({commit}, {data, type_id}) {
            commit('SET_COMMUNALS', {data, type_id})
        },
        getCommunal({commit}, type_id) {
            commit('GET_COMMUNAL', type_id)
        },
        changeType({commit}, type_id) {
            commit('SET_CHANGE_STATE', type_id)
        },
        changeRealty({commit}, realty_id) {
            commit('SET_CHANGE_REALTY', realty_id)
        }
    },
    modules: {},
    getters: {
        getRealty_id: state => {
            return state.realty_id;
        },
        type_id: state => state.type_id,
    }
})
