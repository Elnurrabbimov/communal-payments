import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import MainContent from "../views/MainContent";
import Payment from "../views/Payment";
import UpdaterView from "../views/Updates/UpdaterView";
import i18n from "../i18n";
import AddRealty from "../components/AddRealty";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect: `/${i18n.locale}/communal-payments-new`
    },
    {
        path: '/:lang/communal-payments-new',
        component: {
            render(c) {
                return c('router-view')
            }
        },
        children: [
            {
                path: 'show-realty/:id?',
                name: 'communals',
                component: Home,
                props: true
            },
            {
                path: 'view/:id/:type_id',
                name: 'communalsInfo',
                component: MainContent,
                props: true,
            },
            {
                path: 'payment-view/:communal_id/:id/:type_id',
                name: 'payment',
                component: Payment,
                props: true,
            },
            {
                path: 'update/:id/:type_id',
                name: 'communalUpdate',
                component: UpdaterView,
                props: true,
            },
            {
                path: 'realtyAdd',
                name: 'addRealty',
                component: AddRealty,
                props: true,
            },
        ]
    }
]

const router = new VueRouter({
    routes,
    mode: "history"
})

export default router
