import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '/src/assets/css/new_table.css'
import '/src/assets/js/index'

Vue.config.productionTip = false
import i18n from './i18n'

router.beforeEach((to, from, next) => {
    let language = to.params.lang;
    if (!language) {
        language = 'ru'
    }

    i18n.locale = language
    next()
})

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app')
